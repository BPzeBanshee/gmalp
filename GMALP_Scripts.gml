#define AInit
///AInit()
globalvar GMALP_DLL,
GMALP_Call,GMALP_Init,GMALP_Update,GMALP_Free,GMALP_Version,
GMALP_SourceLoad,GMALP_SourceFree,GMALP_SourceFreeAll,GMALP_SourceGetFormatData,
GMALP_SourceAddLoopPoint,GMALP_SourceGetNumLoopPoints,GMALP_SourceGetLoopPointData,
GMALP_SourceClearLoopPoint,GMALP_SourceGetLength,
GMALP_SourceGetRepeat,GMALP_SourceSetRepeat,
GMALP_StreamIsSeekable,GMALP_StreamIsPaused,
GMALP_StreamPlay,GMALP_StreamStop,GMALP_StreamPause,GMALP_StreamResume,GMALP_StreamIsPlaying,
GMALP_StreamGetLength,GMALP_StreamGetPosition,GMALP_StreamGetVolume,GMALP_StreamGetPitch,
GMALP_StreamGetRepeat,GMALP_StreamGetPan,GMALP_StreamGetPitch,GMALP_StreamSetPosition,
GMALP_StreamSetVolume,GMALP_StreamSetPitch,GMALP_StreamSetRepeat,GMALP_StreamSetPan,
GMALP_StreamSetPitch,GMALP_StreamStopAll,GMALP_GetDevices,GMALP_GetFileFormats;
GMALP_DLL = "GMALP.dll";
GMALP_Call = dll_cdecl;

if !file_exists(GMALP_DLL) return -1; // File Not Found
if os_browser != browser_not_a_browser return -2; // Running In Browser
if os_type != os_windows return -3; // Running on Android etc without GMALP port

// DLL HANDLE COMMANDS
GMALP_Init = external_define(GMALP_DLL, "AInit", GMALP_Call, ty_real, 0);
GMALP_Update = external_define(GMALP_DLL, "AUpdate", GMALP_Call, ty_real, 1, ty_real);
GMALP_Free = external_define(GMALP_DLL, "AFree", GMALP_Call, ty_real, 0);
GMALP_Version = external_define(GMALP_DLL, "AVersion", GMALP_Call, ty_string, 0);
GMALP_GetDevices = external_define(GMALP_DLL, "AGetSupportedAudioDevices", GMALP_Call, ty_string, 0);
GMALP_GetFileFormats = external_define(GMALP_DLL, "AGetSupportedFileFormats", GMALP_Call, ty_string, 0);

// SOURCE COMMANDS
GMALP_SourceLoad = external_define(GMALP_DLL, "ASourceLoad", GMALP_Call, ty_real, 1, ty_string);
GMALP_SourceFree = external_define(GMALP_DLL, "ASourceFree", GMALP_Call, ty_real, 1, ty_real);
GMALP_SourceFreeAll = external_define(GMALP_DLL, "ASourceFreeAll", GMALP_Call, ty_real, 0);
GMALP_SourceGetFormatData = external_define(GMALP_DLL, "ASourceGetFormatData", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_SourceAddLoopPoint = external_define(GMALP_DLL, "ASourceAddLoopPoint", GMALP_Call, ty_real, 4, ty_real, ty_real, ty_real, ty_real);
GMALP_SourceClearLoopPoint = external_define(GMALP_DLL, "ASourceClearLoopPoint", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_SourceGetLoopPointData = external_define(GMALP_DLL, "ASourceGetLoopPointData", GMALP_Call, ty_real, 3, ty_real, ty_real, ty_real);
GMALP_SourceGetNumLoopPoints = external_define(GMALP_DLL, "ASourceGetNumLoopPoints", GMALP_Call, ty_real, 1, ty_real);
GMALP_SourceGetLength = external_define(GMALP_DLL, "ASourceGetLength", GMALP_Call, ty_real, 1, ty_real);
GMALP_SourceGetRepeat = external_define(GMALP_DLL, "ASourceGetRepeat", GMALP_Call, ty_real, 1, ty_real);
GMALP_SourceSetRepeat = external_define(GMALP_DLL, "ASourceSetRepeat", GMALP_Call, ty_real, 2, ty_real, ty_real);

// STREAM COMMANDS
GMALP_StreamPlay = external_define(GMALP_DLL, "AStreamPlay", GMALP_Call, ty_real, 3, ty_real, ty_real, ty_real);
GMALP_StreamStop = external_define(GMALP_DLL, "AStreamStop", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamStopAll = external_define(GMALP_DLL, "AStreamStopAll", GMALP_Call, ty_real, 0);
GMALP_StreamPause = external_define(GMALP_DLL, "AStreamPause", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamResume = external_define(GMALP_DLL, "AStreamResume", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamIsPlaying = external_define(GMALP_DLL, "AStreamIsPlaying", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamIsSeekable = external_define(GMALP_DLL, "AStreamIsSeekable", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamIsPaused = external_define(GMALP_DLL, "AStreamIsPaused", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetLength = external_define(GMALP_DLL, "AStreamGetLength", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetPosition = external_define(GMALP_DLL, "AStreamGetPosition", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetVolume = external_define(GMALP_DLL, "AStreamGetVolume", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetRepeat = external_define(GMALP_DLL, "AStreamGetRepeat", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetPan = external_define(GMALP_DLL, "AStreamGetPan", GMALP_Call, ty_real, 1, ty_real);
GMALP_StreamGetPitch = external_define(GMALP_DLL, "AStreamGetPitch", GMALP_Call, ty_real, 1, ty_real);

GMALP_StreamSetPosition = external_define(GMALP_DLL, "AStreamSetPosition", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_StreamSetVolume = external_define(GMALP_DLL, "AStreamSetVolume", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_StreamSetRepeat = external_define(GMALP_DLL, "AStreamSetRepeat", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_StreamSetPan = external_define(GMALP_DLL, "AStreamSetPan", GMALP_Call, ty_real, 2, ty_real, ty_real);
GMALP_StreamSetPitch = external_define(GMALP_DLL, "AStreamSetPitch", GMALP_Call, ty_real, 2, ty_real, ty_real);

var e;
e = external_call(GMALP_Init);
return e;

#define AUpdate
///AUpdate()
var e;
e = external_call(GMALP_Update, room_speed);
return e;



#define AFree
///AFree()
external_call(GMALP_Free);
external_free(GMALP_DLL);




#define AVersion
///AVersion()
var e;
e = external_call(GMALP_Version);
return e;

#define AGetSupportedAudioDevices
///AGetSupportedAudioDevices()
var e;
e = external_call(GMALP_GetDevices);
return e;

#define AGetSupportedFileFormats
///AGetSupportedFileFormats()
var e;
e = external_call(GMALP_GetFileFormats);
return e;

#define ASourceLoad
///ASourceLoad(file)
if !file_exists(argument0) then return -1;
var e;
e = external_call(GMALP_SourceLoad, argument0);
return e;


#define ASourceFree
///ASourceFree(source)
var e;
e = external_call(GMALP_SourceFree, argument0);
argument0=0;
return e;




#define ASourceFreeAll
///ASourceFreeAll()
var e;
e = external_call(GMALP_SourceFreeAll);
return e;



#define ASourceGetLength
///ASourceGetLength(source_index);
var e;
e = external_call(GMALP_SourceGetLength, argument0);
return e;

#define ASourceGetRepeat
///ASourceGetRepeat(source_index);
var e;
e = external_call(GMALP_SourceGetRepeat, argument0);
return e;

#define ASourceSetRepeat
///ASourceSetRepeat(source_index, repeat);
var e;
e = external_call(GMALP_SourceSetRepeat, argument0, argument1);
return e;

#define ASourceAddLoopPoint
///ASourceAddLoopPoint(source_index,loop_start,loop_end,num_loops);
// use -1 in num_loops/argument3 for infinite looping
var e;
e = external_call(GMALP_SourceAddLoopPoint, argument0, argument1, argument2, argument3);
return e;




#define ASourceClearLoopPoint
///ASourceClearLoopPoint(source_index,loop_index);
if ASourceGetNumLoopPoints(argument0) = 0 then return -1;
var e;
e = external_call(GMALP_SourceClearLoopPoint, argument0, argument1);
return e;




#define ASourceGetLoopPointData
///ASourceGetLoopPointData(source_index,loop_index,datatype);
// datatype 1 = loop start
// datatype 2 = loop end
// datatype 3 = number of loops
var e;
e = external_call(GMALP_SourceGetLoopPointData, argument0, argument1, argument2);
return e;




#define ASourceGetNumLoopPoints
///ASourceGetNumLoopPoints(source_index);
var e;
e = external_call(GMALP_SourceGetNumLoopPoints, argument0);
return e;




#define ASourceGetFormatData
///ASourceGetFormatData(source_index,datatype);
// datatype 0 = number of channels
// datatype 1 = sample rate
var e;
e = external_call(GMALP_SourceGetFormatData, argument0, argument1);
return e;




#define AStreamPlay
///AStreamPlay(source_index,streaming,[volume])
var v; v = 1;
if argument_count > 2 v = argument[2];

var e;
e = external_call(GMALP_StreamPlay, argument[0], argument[1], v);
return e;

#define AStreamStop
///AStreamStop(stream_index)
var e;
e = external_call(GMALP_StreamStop, argument0);
return e;




#define AStreamStopAll
///AStreamStopAll()
var e;
e = external_call(GMALP_StreamStopAll);
return e;



#define AStreamPause
///AStreamPause(stream_index)
var e;
e = external_call(GMALP_StreamPause, argument0);
return e;




#define AStreamResume
///AStreamResume(stream_index)
var e;
e = external_call(GMALP_StreamResume, argument0);
return e;



#define AStreamIsPlaying
///AStreamIsPlaying(stream_index)
var e;
e = external_call(GMALP_StreamIsPlaying, argument0);
return e;

#define AStreamIsPaused
///AStreamIsPaused(stream_index)
var e;
e = external_call(GMALP_StreamIsPaused, argument0);
return e;

#define AStreamIsSeekable
///AStreamIsSeekable(stream_index)
var e;
e = external_call(GMALP_StreamIsSeekable, argument0);
return e;




#define AStreamGetLength
///AStreamGetLength(stream_index);
var e;
e = external_call(GMALP_StreamGetLength, argument0);
return e;




#define AStreamGetPitch
///AStreamGetPitch(stream_index)
var e;
e = external_call(GMALP_StreamGetPitch, argument0);
return e;




#define AStreamSetPitch
///AStreamSetPitch(stream_index,pitch)
var e;
e = external_call(GMALP_StreamSetPitch, argument0, argument1);
return e;




#define AStreamGetPosition
///AStreamGetPosition(stream_index)
var e;
e = external_call(GMALP_StreamGetPosition, argument0);
return e;



#define AStreamSetPosition
///AStreamSetPosition(stream_index,position)
var e;
e = external_call(GMALP_StreamSetPosition, argument0, argument1);
return e;




#define AStreamGetVolume
///AStreamGetVolume(stream_index)
var e;
e = external_call(GMALP_StreamGetVolume, argument0);
return e;




#define AStreamSetVolume
///AStreamSetVolume(stream_index,volume)
var e;
e = external_call(GMALP_StreamSetVolume, argument0, argument1);
return e;




#define AStreamGetRepeat
///AStreamGetRepeat(stream_index)
var e;
e = external_call(GMALP_StreamGetRepeat, argument0);
return e;




#define AStreamSetRepeat
///AStreamSetRepeat(stream_index,repeat)
var e;
e = external_call(GMALP_StreamSetRepeat, argument0, argument1);
return e;




#define AStreamGetPan
///AStreamGetPan(stream_index)
var e;
e = external_call(GMALP_StreamGetPan, argument0);
return e;




#define AStreamSetPan
///AStreamSetPan(stream_index,pan)
var e;
e = external_call(GMALP_StreamSetPan, argument0, argument1);
return e;




