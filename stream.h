#pragma once
#include "audiere.h"
using namespace audiere;
extern AudioDevicePtr device;
class Stream {
public:
    Stream()
    {
        mypos = 0;
        loop = 0;
    }
    ~Stream()
    {
        loopstart.clear();
        loopend.clear();
        totalloops.clear();
        mystream = 0;
    }

    // Init
    //double init(SampleSourcePtr source, double streaming) {
    double init(LoopPointSourcePtr source, double streaming) {
        mystream = OpenSound(device, source.get(), (bool)streaming);
        if (!mystream) { return -1; }
        is_streaming = (bool)streaming;

        source->getFormat(fchannels, fsamplerate, ftype); // get frequency
        numloops = source->getLoopPointCount(); // get number of loop points

        if (numloops == 0) {
            loopstart.push_back(0);
            loopend.push_back(source->getLength());
            totalloops.push_back(-1);
        }
        else {
            int a = 0; int b = 0; int c = 0;
            for (unsigned int i = 0; i < numloops; ++i) {
                source->getLoopPoint(i, a, b, c);
                loopstart.push_back(b);
                loopend.push_back(a);
                totalloops.push_back(c);
            }
        }
        return 0;
    }

    double update(double room_speed) {
        if (!mystream) { return -1; }
        if (is_playing()) {
            mypos += round((fsamplerate / round(room_speed)) * get_pitch());
            if (mypos >= loopend[loop] && mystream->getRepeat()) {
                if (!is_streaming) {
                    mypos = 0;
                }
                else {
                    mypos = loopstart[loop];
                }

                // Set loop point totals according to loop settings
                if (totalloops[loop] > 0) {
                    totalloops[loop] -= 1;
                }
                if (totalloops[loop] <= 0) {
                    if (loop < numloops - 1) {
                        loop += 1;
                    }
                }
            }
        }
        else if (!isPaused) {
            mypos = 0;
        }
        return 0;
    }

    // Play sound from stream
    double play_sound() {
        if (!mystream) { return -1; }
        //mystream->setVolume(1.0f);
        //mystream->setRepeat(true);
        mystream->play();
        isPaused = false;
        return 0;
    }

    // Pauses stream
    double pause_sound() {
        if (!mystream) { return -1; }
        mystream->stop();
        isPaused = true;
        return 0;
    }

    // Resumes stream
    double resume_sound() {
        if (!mystream) { return -1; }
        mystream->play();
        if (isPaused == true) {
            mystream->setPosition(mypos);
        }
        else {
            mystream->setPosition(0);
        }
        isPaused = false;
        return 0;
    }

    // Stops stream
    double stop_sound() {
        if (!mystream) { return -1; }
        mystream->stop();
        mystream->reset();
        mypos = 0;
        isPaused = false;
        return 0;
    }

    // Is it playing or not?
    double is_playing() {
        if (!mystream) { return -1; }
        return (double)mystream->isPlaying();
    }

    // Is it 'paused'?
    double is_paused() {
        if (!mystream) { return -1; }
        return (double) isPaused;
    }

    // Is it seekable? (apparently some aren't)
    double is_seekable() {
        if (!mystream) { return -1; }
        return (double)mystream->isSeekable();
    }

    double get_length() {
        if (!mystream) { return -1; }
        return (double)mystream->getLength();
    }

    /**
     * Gets the current position of the sound file in samples
     * @return position
     */
    double get_position() {
        if (!mystream) { return -1; }
        //return (double)mystream->getPosition();
        if (is_streaming) {
            return (double)mypos;
        }
        else {
            return (double)mystream->getPosition();
        }
    }

    /**
     * Gets the current repeat status of the sound file
     * @return repeat status
     */
    double get_repeat() {
        if (!mystream) { return -1; }
        return (double)mystream->getRepeat();
    }

    /**
     * Gets the current volume of the sound file
     * @return volume
     */
    double get_volume() {
        if (!mystream) { return -1; }
        return (double)mystream->getVolume();
    }

    /**
     * Gets the current pan of the sound file
     * @return pan
     */
    double get_pan() {
        if (!mystream) { return -1; }
        return (double)mystream->getPan();
    }

    /**
     * Gets the current pitch of the sound file
     * @return pan
     */
    double get_pitch() {
        if (!mystream) { return -1; }
        return (double)mystream->getPitchShift();
    }

    /**
     * Sets the current position of the sound file in samples
     */
    double set_position(double s) {
        if (!mystream) { return -1; }
        mystream->setPosition((unsigned int)s);
        mypos = (unsigned int)s;
        return 0;
    }

    /**
     * Sets the repeat status of the sound file
     */
    double set_repeat(double s) {
        if (!mystream) { return -1; }
        mystream->setRepeat((bool)s);
        return 0;
    }

    /**
     * Sets the volume of the sound file
     */
    double set_volume(double s) {
        if (!mystream) { return -1; }
        mystream->setVolume((float)s);
        return 0;
    }

    /**
     * Sets the pan of the sound file
     */
    double set_pan(double s) {
        if (!mystream) { return -1; }
        mystream->setPan((float)s);
        return 0;
    }

    /**
     * Sets the pan of the sound file
     */
    double set_pitch(double s) {
        if (!mystream) { return -1; }
        mystream->setPitchShift((float)s);
        return 0;
    }

private:
    // declare variables here if needed
    OutputStreamPtr mystream;

    // self-managed position variable
    std::vector<unsigned int> loopstart;
    std::vector<unsigned int> loopend;
    std::vector<unsigned int> totalloops;
    unsigned int mypos = 0;
    unsigned int numloops = 0;
    unsigned int loop = 0;
    bool is_streaming = false;
    bool isPaused = false;

    // get channel data
    int fchannels = 2;
    int fsamplerate = 44100;
    SampleFormat ftype = SF_U8;
};
