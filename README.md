# GMALP #
## A Game Maker wrapper of Audiere with loop point support ##
###  Created and primarily maintained by BPzeBanshee ###

This DLL utilizes the poikilos fork of Audiere v1.10.1, cfr: https://github.com/poikilos/Audiere

Special thanks to terinjokes and his modernised Mixere for compiling assistance, cfr: https://github.com/terinjokes/Mixere 

## FAQ ##

### What does GMALP stand for? ###

Game Maker Audiere with Loop Points. Very unoriginal and uninspired, I know.
I started with "GMA" but another project took that one, so this is GMALP.

### The name kinda rings a bell... ###

There's been two Audiere wrapper projects up to now, GMA and XeAudiere.
Neither of them have made use of the LoopPointSourcePtr object in Audiere's library
and so do not support proper loop points. They're also both AFAIK closed-source.

### Okay that's great, what's the point of this? ###

In 2024, with GM having loop point support out of the box, not a lot. You can load WAVs
without having to wrangle around the audio_buffer functions but the DLL wasn't *really*
designed with sfx in mind so YMMV, and it predates GM's advancements by a decade.
This project was originally made circa 2016 for the Grybanser Fox projects I was working on.

I'd been frustrated for a very long time about the lack of decent audio extensions
for Game Maker that allow for loop point support - essentially the ability to set some
values and have the music in question loop back to a certain point. Great for stuff with
intro pieces, for example. Back then, the only extension that I knew of that supported this
was GMFMODSimple, whose license was questionable.

Some others have manual commands for setting position of a track, however due to Game Maker's 
code running on a per-step basis when it really needs immediate running, this means that 
attempting to manually loop a track this way eventually leads to either a gap (too late) 
or a stutter (too early). So I made my own.

### Okay, it supports making a loop point. What else? ###

Not just *a* loop point, *multiple* loop points. 

Apparently it can play Ogg Vorbis, MP3, FLAC, uncompressed WAV, AIFF, MOD, S3M, XM, and IT files.
I've only tested OGG and WAV, the rest I leave to you to figure out.

It currently works out of the box for Game Maker Studio 1.4.9999.
The 32-bit version will work as early back as Game Maker 8.0, although you'll need to edit the
file checking stuff in AInit(), and the 64-bit version will work fine on Studio 2022.2 providing 
you rename the DLLs appropriately because GM doesn't have 32/64 differentation in their IDE yet 
(otherwise I'd make a proper extension library for it to handle that crap).

### So is it just you working on this? ###

For now, yes. I'd really appreciate some support from people who actually know what
they're doing with C++ and GM, however, so that this can extend from being just a
loop point music player into a true wrapper for Audiere - including its functions
to play multiple sound effects and generate tones/pink noise/etc.

### How do I use this? ###

For simple users, there is an example Studio project that comes included with the
main download which is ready-to-go and should give you a good idea of what this project
is capable of. Users familiar with GM and other instance-based audio engines like GMFMODSimple
should feel right at home with this (I hope).

### Anything else I should know? ###

Audiere, and therefore GMALP itself is built around the concept of sources and streams.
You create a source from a file, create a stream from the source and then manage
that stream. The stream takes ownership of the source so connecting multiple streams
to a source leads to funky consequences (cue "Don't cross the streams!.wav").

Regarding the multiple loop points, Audiere has an internal sorted list of loop points
that is applied to whenever you define a loop point. Because it's automatically sorted it doesn't
return any indexes for you to use though, so you have to go by the order of which you created
said index to delete or get information back from it. For instance, the first loop point will
be index 0, if you make another one then delete the first, the second loop point will become
index 0.

### Special Thanks to ###
Double thanks to terinjokes @ https://github.com/terinjokes/Mixere for his vs2022 sln, I literally
could not have updated this for Windows at all without it as CodeBlocks was proving to be a disaster

Everyone at the GMC forums for putting up with me, especially icuurd12b42
Everyone at shmups.system11.org for putting up with me, especially that polish guy