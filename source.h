#pragma once
#include "audiere.h"
using namespace audiere;
extern AudioDevicePtr device;
class Source {
public:
	LoopPointSourcePtr source;
	//SampleSourcePtr source;
	Source()
	{
		fchannels = 0;
		fsamplerate = 0;
		loopnum = 0;
		loopstart = 0;
		loopend = 1;
	}
	~Source()
	{
		source = 0;
	}

	/********************** Wrapper functions for Audiere *****************/

	/**
	 * Creates the loop point sample source type
	 * @param file
	 * @return the index of the sample source
	 */
	double create_loop_point_source(char* file) {
		//source = OpenSampleSource(file);
		source = CreateLoopPointSource(file);
		if (!source) { return -1; }
		return 0;
	}

	double get_format(double type) {
		int fchannels{};
		int fsamplerate{};
		SampleFormat ftype{};
		source->getFormat(fchannels, fsamplerate, ftype);
		if (type == 0) {
			return (double)fchannels;
		}
		if (type == 1) {
			return (double)fsamplerate;
		}
		return -1;
	}

	/**
	 * Adds a loop point to the stream.  If a loop point at 'location'
	 * already exists, the new one replaces it.  Location and target are
	 * clamped to the actual length of the stream.
	 *
	 * @param location   frame where loop occurs
	 * @param target     frame to jump to after loop point is hit
	 * @param loopCount  number of times to execute this jump.
	 */
	double add_loop_point(double loop_end, double loop_start, double num_loops) {
		source->addLoopPoint((int)loop_end, (int)loop_start, (int)num_loops);
		return 0;
	}

	/**
	 * Removes the loop point of index specified
	 * @param loop index
	 */
	double clear_loop_point(double index) {
		source->removeLoopPoint((int)index);
		return 0;
	}

	int load_loop_point_data(int index) {
		return source->getLoopPoint(index, loopstart, loopend, loopnum);
	}

	double get_loop_point_data(double index, double type) {
		int e;
		e = load_loop_point_data((int)index);
		if (e == 1) {
			if (type == 0) {
				return (double)loopstart;
			}
			if (type == 1) {
				return (double)loopend;
			}
			if (type == 2) {
				return (double)loopnum;
			}
		}
		return -1;
	}

	/**
	 * Gets the amount of loop points added to the source
	 * @return number of loop points
	 */
	double get_loop_point_count() {
		return (double)source->getLoopPointCount();
	}

	/**
	 * Resets the loop source (clearing loop points, position and repeat)
	 */
	double reset_source() {
		source->reset();
		return 0;
	}

	/**
	 * Gets the length of the sound file in samples
	 * @return length
	 */
	double get_length() {
		return (double)source->getLength();
	}

	/**
	 * Gets the current position of the sound file in samples
	 * @return position
	 */
	double get_position() {
		return (double)source->getPosition();
	}

	/**
	 * Gets the current repeat status of the sound file
	 * @return repeat status
	 */
	double get_repeat() {
		return (double)source->getRepeat();
	}

	/**
	 * Sets the current position of the sound file in samples
	 */
	double set_position(double s) {
		source->setPosition((int)s);
		return 0;
	}

	/**
	 * Sets the repeat status of the sound file
	 */
	double set_repeat(double s) {
		source->setRepeat((bool)s);
		return 0;
	}
private:
	int loopstart = 0;
	int loopend = 1;
	int loopnum = 0;
	int fchannels = 2;
	int fsamplerate = 44100;
	SampleFormat ftype = SF_U8;
};
