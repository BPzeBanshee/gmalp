///ASourceGetLoopPointData(source_index,loop_index,datatype);
// datatype 1 = loop start
// datatype 2 = loop end
// datatype 3 = number of loops
var e;
e = external_call(GMALP_SourceGetLoopPointData, argument0, argument1, argument2);
return e;
