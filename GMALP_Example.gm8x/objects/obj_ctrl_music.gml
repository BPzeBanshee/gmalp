#define Create_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
// INITIALISATION
a = AInit();
if a < 0
    {
    show_message("AInit(): failed to initialise, exiting...");
    game_end();
    }

// DLL VERSION STRING
version = AVersion();

// Info getters
info_playing = false;
info_paused = false;
info_volume = 0;
info_pos = 0;
info_seek = 0;
info_pitch = 0;
info_pan = 0;
info_repeat = 0;
info_repeat_s = 0;
info_channels = 0;

show_debug_message("Supported Audio Devices: "+string(AGetSupportedAudioDevices()));
show_debug_message("Supported File Formats: "+string(AGetSupportedFileFormats()));

// CALL EVENTS
mode = 0;
event_user(0);

// REAL FPS DISPLAY
rfps_avg = 0;
rfps_str = "----";
alarm[0] = 60;
#define Alarm_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Real FPS Display
rfps_str = string(round(rfps_avg/60));
rfps_avg = 0;
alarm[0] = 60;
#define Alarm_1
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Fade in
var v;
v = AStreamGetVolume(mystream) + 0.005;
//show_debug_message(string(v));
if v < 1 alarm[1] = 1 else v = 1;
AStreamSetVolume(mystream,v);
#define Alarm_2
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Fade out
var v;
v = AStreamGetVolume(mystream) - 0.005;
//show_debug_message(string(v));
if v > 0
    {
    AStreamSetVolume(mystream,v);
    alarm[2] = 1;
    }
else AStreamStop(mystream);
#define Step_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Control Code
// GM-side counter that seems to properly match what's playing
AUpdate();

// Info getters
info_playing = AStreamIsPlaying(mystream);
info_volume = AStreamGetVolume(mystream);
info_paused = AStreamIsPaused(mystream);
info_pos = AStreamGetPosition(mystream);
info_seek = AStreamIsSeekable(mystream);
info_pitch = AStreamGetPitch(mystream);
info_pan = AStreamGetPan(mystream);
info_repeat_s = AStreamGetRepeat(mystream);
if info_playing
    {
    // Pitch Shift Functionality
    if keyboard_check(ord("Z")) 
    AStreamSetPitch(mystream,2)
    else if keyboard_check(ord("X"))
    AStreamSetPitch(mystream,0.5)
    else AStreamSetPitch(mystream,1);
    
    // Pan test
    if keyboard_check_direct(vk_lshift)
    AStreamSetPan(mystream,-1)
    else if keyboard_check_direct(vk_rshift)
    AStreamSetPan(mystream,1)
    else AStreamSetPan(mystream,0);
    
    // Fade out
    if keyboard_check_pressed(ord("C")) 
    && alarm[1] == -1
    && alarm[2] == -1 
    alarm[2] = 1;
    
    if keyboard_check_pressed(ord("V"))
        {
        AStreamSetRepeat(mystream,!AStreamGetRepeat(mystream));
        }
    }

// Pause functionality
if keyboard_check_pressed(vk_space)
    {
    if AStreamIsPlaying(mystream)
        {
        AStreamPause(mystream);
        }
    else
        {
        if AStreamIsPaused(mystream)
        AStreamResume(mystream)
        else 
            {
            mystream = AStreamPlay(myfile,0,1);
            AStreamSetRepeat(mystream,false);
            }
        }
    }

// Switch song
if keyboard_check_pressed(vk_enter)
    {
    mode += 1; if mode > 2 then mode = 0;
    AStreamStop(mystream);
    ASourceFree(myfile);
    event_user(mode);
    }
if keyboard_check_pressed(vk_backspace)
    {
    //show_message(AGetSupportedFileFormats());
    var f; f = get_open_filename("*.wav,*.ogg","");
    if f != ""
        {
        AStreamStop(mystream);
        ASourceFree(myfile);
        myfile = ASourceLoad(f);
        event_user(3);
        }
    }
    
// Kill game
if keyboard_check_pressed(vk_escape) then game_end();

rfps_avg += fps_real;
#define Other_3
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Free all data on exit
if a > -1 // checking Audiere was loaded OK in the first place
    {
    AStreamStopAll(); // Stop all streams
    ASourceFreeAll(); // Free all sources
    AFree();          // and finally free the DLL
    }
#define Other_10
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Load test0.ogg

// CREATE SOURCE AND MANAGE IT
var ff; ff = "sounds/10.wav";
myfile = ASourceLoad(ff);
ASourceSetRepeat(myfile,false);
//show_message("ASourceGetRepeat(myfile) :"+string(ASourceGetRepeat(myfile)));
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// No loop points needed here but we'll still report source length
info_length = ASourceGetLength(myfile);

// Get info (numloops should return 0, but channels/samplerate)
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);
info_repeat = ASourceGetRepeat(myfile);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,0,1);
//alarm[1] = 1;
AStreamSetRepeat(mystream,false);
info_repeat_s = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);
#define Other_11
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Load test1.ogg

// CREATE SOURCE AND MANAGE IT
var ff; ff = "sounds/test1.ogg";
myfile = ASourceLoad(ff);
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// Add loop point
info_length = ASourceGetLength(myfile);
ASourceAddLoopPoint(myfile,info_length*0.1333335816614642,info_length,-1);
mydata[0,0] = ASourceGetLoopPointData(myfile,0,0);// loop point location
mydata[0,1] = ASourceGetLoopPointData(myfile,0,1);// loop point target
mydata[0,2] = ASourceGetLoopPointData(myfile,0,2);// number of times to loop

// Get info
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,true);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);
#define Other_12
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Load test2.ogg

// CREATE SOURCE AND MANAGE IT
var ff; ff = "sounds/test2.ogg";
myfile = ASourceLoad(ff);
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// Set TWO Loop Points
info_length = ASourceGetLength(myfile);
ASourceAddLoopPoint(myfile,240,123075,2);
ASourceAddLoopPoint(myfile,info_length*0.6670999130341582,info_length,-1);
mydata[0,0] = ASourceGetLoopPointData(myfile,0,0); // Loop point location
mydata[0,1] = ASourceGetLoopPointData(myfile,0,1); // Loop point target
mydata[0,2] = ASourceGetLoopPointData(myfile,0,2); // Amount of times to loop
mydata[1,0] = ASourceGetLoopPointData(myfile,1,0);
mydata[1,1] = ASourceGetLoopPointData(myfile,1,1);
mydata[1,2] = ASourceGetLoopPointData(myfile,1,2);

// Get additional info
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,true);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);
#define Other_13
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Load user-defined file
// See step event
// No loop points needed here but we'll still report source length
info_length = ASourceGetLength(myfile);

// Get info (numloops should return 0, but channels/samplerate)
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,false);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);
#define Draw_0
/*"/*'/**//* YYD ACTION
lib_id=1
action_id=603
applies_to=self
*/
/// Draw code
draw_set_font(fnt_default);
draw_set_color(c_white);
draw_set_halign(fa_left);

var bx,by,bz; bx = 5; by = 5; bz = 12;
draw_text(bx,by,"Audiere v"+string(version)+" loaded"); by+=bz;
by+=bz;

draw_text(bx,by,"*** SOURCE INFO ***"); by+=bz;
draw_text(bx,by,string(info_channels)+" channels, "+string(info_samplert)+" Hz"); by+=bz;
draw_text(bx,by,"Length (from source): "+string(info_length)+" samples"); by+=bz;
draw_text(bx,by,"Repeating: "+string(info_repeat));by+=bz;
by+=bz;

draw_text(bx,by,"*** LOOP INFO ***"); by+=bz;
if info_numloops > 0
    {
    var str,str2,i;
    for (i=0; i<info_numloops; i+=1)
        {
        if mydata[i,2] == -1 then str2 = "infinitely" else str2 = string(mydata[i,2])+" time";
        if mydata[i,2] > 1 then str2 += "s";
        str = string(mydata[i,0])+", target: "+string(mydata[i,1])+", looping "+string(str2);
        draw_text(bx,by,"Loop "+string(i)+" location: "+string(str));
        by+=bz;
        }
    }
else
    {
    draw_text(bx,by,"No loop points set for this track.");by+=bz;
    }
by+=bz;

draw_text(bx,by,"*** STREAM INFO ***"); by+=bz;
draw_text(bx,by,"Playing now: "+string(info_playing)); by+=bz;
draw_text(bx,by,"Volume: "+string(info_volume)); by+=bz;
draw_text(bx,by,"Paused: "+string(info_paused)); by+=bz;
draw_text(bx,by,"Seekable: "+string(info_seek)); by+=bz;
draw_text(bx,by,"Repeating: "+string(info_repeat_s)); by+=bz;
draw_text(bx,by,"Pitch Shift: "+string(info_pitch)); by+=bz;
draw_text(bx,by,"Pan: "+string(info_pan)); by+=bz;
draw_text(bx,by,"Length (from stream): "+string(info_length2)+" samples"); by+=bz;
draw_text(bx,by,"Position (from stream): "+string(info_pos)+" samples"); by+=bz;

by = room_height-12-(12*5);
draw_text(5,by,"Z/X: alter pitch"); by+=bz;
draw_text(5,by,"C: fade out, V: toggle repeat"); by+=bz;
draw_text(5,by,"SHIFT keys to alter pan"); by+=bz;
draw_text(5,by,"SPACE to pause/resume"); by+=bz;
draw_text(5,by,"ENTER to change tracks"); by+=bz;

draw_set_halign(fa_right);
draw_text(room_width,room_height-15,string(rfps_str)+ " RFPS");
draw_text(room_width,room_height-27,string(fps)+" FPS");
