#include <windows.h>
#define GMEXPORT extern "C"  __declspec (dllexport)
#include <string>
#include <vector>
#include "audiere.h"
#include "source.h"
#include "stream.h"
using namespace audiere;

// INIT GLOBAL VARS
std::vector<Source*> Sources;
std::vector<unsigned int> OpenSourceSpots;
std::vector<Stream*> Streams;
std::vector<unsigned int> OpenStreamSpots;

AudioDevicePtr device;

/***** CREATE/DELETE FUNCTIONS *****/
int InitSource() {
    int ind;
    if (OpenSourceSpots.empty()) {
        ind = Sources.size();
        Sources.push_back(new Source());
    }
    else {
        ind = OpenSourceSpots.back();
        OpenSourceSpots.pop_back();
        Sources[ind] = new Source();
    }
    return ind;
}

int FreeSource(double i) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        delete Sources[ind];
        Sources[ind] = NULL; //safety measure
        OpenSourceSpots.push_back(ind);
    }
    return 0;
}

int FreeAllSources() {
    for (unsigned int i = 0; i < Sources.size(); i++) {
        FreeSource((double)i);
    }
    return 0;
}

int InitStream() {
    int ind;
    if (OpenStreamSpots.empty()) {
        ind = Streams.size();
        Streams.push_back(new Stream());
    }
    else {
        ind = OpenStreamSpots.back();
        OpenStreamSpots.pop_back();
        Streams[ind] = new Stream();
    }
    return ind;
}

int FreeStream(double i) {
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        delete Streams[ind];
        Streams[ind] = NULL; //safety measure
        OpenStreamSpots.push_back(ind);
    }
    return 0;
}

/*** MAIN EXTERNAL FUNCTIONS ***/
GMEXPORT double AInit() {
    device = OpenDevice();
    if (!device) { return -1; }
    return 0;
}

GMEXPORT double AUpdate(double roomspeed) {
    if (!device) { return -1; }
    for (unsigned int i = 0; i < Streams.size(); i++) {
        if (Streams[i] != NULL) {
            Streams[i]->update(roomspeed);
        }
    }
    return 0;
}

GMEXPORT double AFree() {
    FreeAllSources();
    device = 0;
    return 0;
}

GMEXPORT const char* AVersion() {
    static std::string a;
    a = audiere::GetVersion();
    a.append(" (GMALP 2024.10.5)");
    const char *C = a.c_str();
    return C;
}

GMEXPORT const char* AGetSupportedFileFormats(){
	return audiere::hidden::AdrGetSupportedFileFormats();
}

GMEXPORT const char* AGetSupportedAudioDevices(){
    return audiere::hidden::AdrGetSupportedAudioDevices();
}

/*** SOURCE FUNCTIONS ***/
GMEXPORT double ASourceLoad(char* file) {
    unsigned int ind = InitSource();
    double result;
    result = Sources[ind]->create_loop_point_source(file);
    if (result < 0) {
        return result;
    }
    return (double)ind;
}

GMEXPORT double ASourceFree(double i) {
    FreeSource(i);
    return 0;
}

GMEXPORT double ASourceFreeAll() {
    FreeAllSources();
    return 0;
}

GMEXPORT double ASourceAddLoopPoint(double i, double loop_start, double loop_end, double num_loops) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        Sources[ind]->add_loop_point(loop_end, loop_start, num_loops);
        return 0;
    }
    return 1;
}

GMEXPORT double ASourceGetFormatData(double i, double l) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_format(l);
    }
    return -2;
}

GMEXPORT double ASourceClearLoopPoint(double i, double loop_index) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        Sources[ind]->clear_loop_point(loop_index);
        return 0;
    }
    return 1;
}

GMEXPORT double ASourceGetLoopPointData(double i, double l, double t) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_loop_point_data(l, t);
    }
    return -2;
}

GMEXPORT double ASourceGetNumLoopPoints(double i) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_loop_point_count();
    }
    return 1;
}

GMEXPORT double ASourceGetLength(double i) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_length();
    }
    return 1;
}

GMEXPORT double ASourceGetRepeat(double i) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_repeat();
    }
    return 1;
}

GMEXPORT double ASourceSetRepeat(double i, double r) {
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        Sources[ind]->set_repeat(r);
        return 0;
    }
    return 1;
}

/*GMEXPORT double ASourceGetPosition(double i) {
    if (!device) {return -1;}
    unsigned int ind = (unsigned int)i;
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        return Sources[ind]->get_position();
    }
    return -2;
}*/

/*** STREAM FUNCTIONS ***/
GMEXPORT double AStreamPlay(double i, double s, double v = 1.0) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    unsigned int ind2 = InitStream();
    if (Sources.size() > ind && Sources[ind] != NULL) { //safety measure
        Streams[ind2]->init(Sources[ind]->source.get(), s);
        Streams[ind2]->set_volume(v);
        Streams[ind2]->play_sound();
        return (double)ind2;
    }
    return -2;
}

GMEXPORT double AStreamStop(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->stop_sound();
        FreeStream(i);
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamStopAll() {
    if (!device) { return -1; }
    for (unsigned int i = 0; i < Streams.size(); i++) {
        if (Streams[i] != NULL) {
            Streams[i]->stop_sound();
            FreeStream((double)i);
        }
    }
    return 0;
}

/*GMEXPORT double AStreamUpdate(double i, double roomspeed) {
    if (!device) {return -1;}
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) {
        return Streams[ind]->update(roomspeed);
    }
    return -1;
}*/

GMEXPORT double AStreamIsPlaying(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->is_playing();
    }
    return -2;
}

GMEXPORT double AStreamIsSeekable(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->is_seekable();
    }
    return -2;
}

GMEXPORT double AStreamIsPaused(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->is_paused();
    }
    return -2;
}

GMEXPORT double AStreamPause(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->pause_sound();
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamResume(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->resume_sound();
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamGetLength(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_length();
    }
    return -2;
}

GMEXPORT double AStreamGetPosition(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_position();
    }
    return -2;
}

GMEXPORT double AStreamGetRepeat(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_repeat();
    }
    return -2;
}

GMEXPORT double AStreamGetVolume(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_volume();
    }
    return -2;
}

GMEXPORT double AStreamGetPan(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_pan();
    }
    return -2;
}

GMEXPORT double AStreamGetPitch(double i) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        return Streams[ind]->get_pitch();
    }
    return -2;
}

GMEXPORT double AStreamSetPosition(double i, double p) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->set_position(p);
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamSetRepeat(double i, double r) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->set_repeat(r);
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamSetVolume(double i, double v) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->set_volume(v);
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamSetPan(double i, double p) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->set_pan(p);
        return 0;
    }
    return -2;
}

GMEXPORT double AStreamSetPitch(double i, double p) {
    if (!device) { return -1; }
    unsigned int ind = (unsigned int)i;
    if (Streams.size() > ind && Streams[ind] != NULL) { //safety measure
        Streams[ind]->set_pitch(p);
        return 0;
    }
    return -2;
}

// DLL FUNCTIONS
BOOL WINAPI DllMain(
    HANDLE hinstDLL,
    DWORD dwReason,
    LPVOID lpvReserved
)
{
    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
