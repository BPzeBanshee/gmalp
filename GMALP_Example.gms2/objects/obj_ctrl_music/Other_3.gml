/// @desc Free all data on exit
if a > -1 // checking Audiere was loaded OK in the first place
    {
    AStreamStopAll(); // Stop all streams
    ASourceFreeAll(); // Free all sources
    AFree();          // and finally free the DLL
    }

