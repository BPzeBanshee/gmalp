/// @desc Load test1.ogg

// CREATE SOURCE AND MANAGE IT
var ff = "sounds/test1.ogg";
myfile = ASourceLoad(ff);
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// Add loop point
info_length = ASourceGetLength(myfile);
ASourceAddLoopPoint(myfile,info_length*0.1333335816614642,info_length,-1);
mydata[0,0] = ASourceGetLoopPointData(myfile,0,0);// loop point location
mydata[0,1] = ASourceGetLoopPointData(myfile,0,1);// loop point target
mydata[0,2] = ASourceGetLoopPointData(myfile,0,2);// number of times to loop

// Get info
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,true);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);

