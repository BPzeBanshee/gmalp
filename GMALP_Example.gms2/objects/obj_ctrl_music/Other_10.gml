/// @desc Load test0.ogg

// CREATE SOURCE AND MANAGE IT
var ff = "sounds/10.wav";
myfile = ASourceLoad(ff);
ASourceSetRepeat(myfile,false);
//show_message("ASourceGetRepeat(myfile) :"+string(ASourceGetRepeat(myfile)));
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// No loop points needed here but we'll still report source length
info_length = ASourceGetLength(myfile);

// Get info (numloops should return 0, but channels/samplerate)
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);
info_repeat = ASourceGetRepeat(myfile);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,0,1);
//alarm[1] = 1;
AStreamSetRepeat(mystream,false);
info_repeat_s = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);

