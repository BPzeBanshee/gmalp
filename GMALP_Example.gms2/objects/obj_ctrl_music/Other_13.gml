/// @desc Load user-defined file
// See step event
// No loop points needed here but we'll still report source length
info_length = ASourceGetLength(myfile);

// Get info (numloops should return 0, but channels/samplerate)
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,false);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);

