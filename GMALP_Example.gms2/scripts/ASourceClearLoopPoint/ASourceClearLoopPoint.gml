/// @desc ASourceClearLoopPoint(source_index,loop_index);
/// @param {Real} source_index
/// @param {Real} loop_index
function ASourceClearLoopPoint(source_index,loop_index) {
	if ASourceGetNumLoopPoints(source_index) == 0 then return -1;
	var e = external_call(GMALP_SourceClearLoopPoint,source_index,loop_index);
	return e;
}