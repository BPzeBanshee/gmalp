/// @desc AStreamResume(stream_index)
/// @param {Real} stream_index
function AStreamResume(stream_index) {
	var e = external_call(GMALP_StreamResume,stream_index);
	return e;
}