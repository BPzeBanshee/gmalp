/// @desc ASourceGetNumLoopPoints(source_index);
/// @param {Real} source_index
function ASourceGetNumLoopPoints(source_index) {
	var e = external_call(GMALP_SourceGetNumLoopPoints,source_index);
	return e;
}