/// @desc AStreamSetVolume(stream_index,volume)
/// @param {Real} stream_index
/// @param {Real} volume
function AStreamSetVolume(stream_index,volume) {
	var e = external_call(GMALP_StreamSetVolume,stream_index,volume);
	return e;
}