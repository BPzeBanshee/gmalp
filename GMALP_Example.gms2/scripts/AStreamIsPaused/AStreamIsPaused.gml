/// @desc AStreamIsPaused(stream_index)
/// @param {Real} stream_index
function AStreamIsPaused(stream_index) {
	var e = external_call(GMALP_StreamIsPaused, stream_index);
	return e;
}