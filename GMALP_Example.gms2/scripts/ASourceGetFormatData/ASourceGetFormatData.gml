/// @desc ASourceGetFormatData(source_index,datatype);
/// @param {Real} source_index
/// @param {Real} datatype
function ASourceGetFormatData(source_index,datatype) {
	// datatype 0 = number of channels
	// datatype 1 = sample rate
	var e = external_call(GMALP_SourceGetFormatData,source_index,datatype);
	return e;
}