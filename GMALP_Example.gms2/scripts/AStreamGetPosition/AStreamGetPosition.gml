/// @desc AStreamGetPosition(stream_index)
/// @param {Real} stream_index
function AStreamGetPosition(stream_index) {
	var e = external_call(GMALP_StreamGetPosition, stream_index);
	return e;
}