/// @desc AStreamSetRepeat(stream_index,_repeat)
/// @param {Real} stream_index
/// @param {Real} _repeat
function AStreamSetRepeat(stream_index,_repeat) {
	var e = external_call(GMALP_StreamSetRepeat,stream_index,_repeat);
	return e;
}