/// @desc AStreamPause(stream_index)
/// @param {Real} stream_index
function AStreamPause(stream_index) {
	var e = external_call(GMALP_StreamPause,stream_index);
	return e;
}