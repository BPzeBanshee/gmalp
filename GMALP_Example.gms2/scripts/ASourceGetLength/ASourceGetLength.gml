/// @desc ASourceGetLength(source_index);
/// @param {Real} source_index
function ASourceGetLength(source_index) {
	var e = external_call(GMALP_SourceGetLength, source_index);
	return e;
}