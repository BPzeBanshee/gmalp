/// @desc ASourceFree(source)
/// @param {Real} source
function ASourceFree(source) {
	var e = external_call(GMALP_SourceFree, source);
	source = 0;
	return e;
}