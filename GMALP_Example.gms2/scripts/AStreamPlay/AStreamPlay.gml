/// @desc AStreamPlay(source_index,streaming,[volume])
/// @param {Real} source_index
/// @param {Real} streaming
/// @param {Real} [volume]
function AStreamPlay(source_index,streaming,volume=1.0) {
	var e = external_call(GMALP_StreamPlay,source_index,streaming,volume);
	return e;
}