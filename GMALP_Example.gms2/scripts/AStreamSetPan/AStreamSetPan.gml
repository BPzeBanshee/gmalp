/// @desc AStreamSetPan(stream_index,pan)
/// @param {Real} stream_index
/// @param {Real} pan
function AStreamSetPan(stream_index,pan) {
	var e = external_call(GMALP_StreamSetPan,stream_index,pan);
	return e;
}