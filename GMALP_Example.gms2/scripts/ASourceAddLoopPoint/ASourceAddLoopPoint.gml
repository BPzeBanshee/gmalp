/// @desc ASourceAddLoopPoint(source_index,loop_start,loop_end,num_loops);
/// @param {Real} source_index
/// @param {Real} loop_start
/// @param {Real} loop_end
/// @param {Real} num_loops
function ASourceAddLoopPoint(source_index,loop_start,loop_end,num_loops) {
	// use -1 in num_loops/argument3 for infinite looping
	var e = external_call(GMALP_SourceAddLoopPoint,source_index,loop_start,loop_end,num_loops);
	return e;
}