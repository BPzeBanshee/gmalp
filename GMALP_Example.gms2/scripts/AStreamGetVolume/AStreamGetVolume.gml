/// @desc AStreamGetVolume(stream_index)
/// @param {Real} stream_index
function AStreamGetVolume(stream_index) {
	var e  = external_call(GMALP_StreamGetVolume, stream_index);
	return e;
}