/// @desc AStreamGetPan(stream_index)
/// @param {Real} stream_index
function AStreamGetPan(stream_index) {
	var e = external_call(GMALP_StreamGetPan, stream_index);
	return e;
}