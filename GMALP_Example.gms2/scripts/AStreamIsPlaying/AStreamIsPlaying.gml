/// @desc AStreamIsPlaying(stream_index)
/// @param {Real} stream_index
function AStreamIsPlaying(stream_index) {
	var e = external_call(GMALP_StreamIsPlaying,stream_index);
	return e;
}