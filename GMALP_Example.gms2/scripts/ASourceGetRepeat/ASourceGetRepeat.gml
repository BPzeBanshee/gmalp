/// @desc ASourceGetRepeat(source_index);
/// @param {Real} source_index
function ASourceGetRepeat(source_index) {
	var e = external_call(GMALP_SourceGetRepeat, source_index);
	return e;
}