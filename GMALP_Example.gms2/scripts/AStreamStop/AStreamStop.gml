/// @desc AStreamStop(stream_index)
/// @param {Real} stream_index
function AStreamStop(stream_index) {
	var e = external_call(GMALP_StreamStop,stream_index);
	return e;
}