/// @desc AStreamGetPitch(stream_index)
/// @param {Real} stream_index
function AStreamGetPitch(stream_index) {
	var e = external_call(GMALP_StreamGetPitch, stream_index);
	return e;
}