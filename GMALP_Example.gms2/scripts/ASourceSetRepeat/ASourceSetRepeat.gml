/// @desc ASourceSetRepeat(source_index, _repeat);
/// @param {Real} source_index
/// @param {Real} _repeat
function ASourceSetRepeat(source_index, _repeat) {
	var e = external_call(GMALP_SourceSetRepeat, source_index, _repeat);
	return e;
}