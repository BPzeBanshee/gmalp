/// @desc ASourceLoad(file)
/// @param {String} file
function ASourceLoad(file) {
	if !file_exists(file) then return -1;
	var e = external_call(GMALP_SourceLoad, file);
	return e;
}