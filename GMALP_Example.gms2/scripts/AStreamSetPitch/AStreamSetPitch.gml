/// @desc AStreamSetPitch(stream_index,pitch)
/// @param {Real} stream_index
/// @param {Real} pitch
function AStreamSetPitch(stream_index,pitch) {
	var e = external_call(GMALP_StreamSetPitch, stream_index,pitch);
	return e;
}