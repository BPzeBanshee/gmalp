/// @desc AStreamIsSeekable(stream_index)
/// @param {Real} stream_index
function AStreamIsSeekable(stream_index) {
	var e = external_call(GMALP_StreamIsSeekable, stream_index);
	return e;
}