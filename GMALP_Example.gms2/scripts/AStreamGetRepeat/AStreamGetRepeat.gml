/// @desc AStreamGetRepeat(stream_index)
/// @param {Real} stream_index
function AStreamGetRepeat(stream_index) {
	var e = external_call(GMALP_StreamGetRepeat, stream_index);
	return e;
}