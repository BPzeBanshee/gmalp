/// @desc ASourceGetLoopPointData(source_index,loop_index,datatype);
/// @param {Real} source_index
/// @param {Real} loop_index
/// @param {Real} datatype
function ASourceGetLoopPointData(source_index,loop_index,datatype) {
	// datatype 1 = loop start
	// datatype 2 = loop end
	// datatype 3 = number of loops
	var e = external_call(GMALP_SourceGetLoopPointData,source_index,loop_index,datatype);
	return e;
}