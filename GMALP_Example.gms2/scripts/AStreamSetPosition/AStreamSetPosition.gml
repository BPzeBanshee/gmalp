/// @desc AStreamSetPosition(stream_index,position)
/// @param {Real} stream_index
/// @param {Real} position
function AStreamSetPosition(stream_index,position) {
	var e = external_call(GMALP_StreamSetPosition,stream_index,position);
	return e;
}