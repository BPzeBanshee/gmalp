/// @desc AStreamGetLength(stream_index);
/// @param {Real} stream_index
function AStreamGetLength(stream_index) {
	var e = external_call(GMALP_StreamGetLength, stream_index);
	return e;
}