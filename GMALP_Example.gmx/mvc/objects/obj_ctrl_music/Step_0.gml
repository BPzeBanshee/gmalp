/// @description  Control Code
// GM-side counter that seems to properly match what's playing
AUpdate();

// Info getters
info_playing = AStreamIsPlaying(mystream);
info_volume = AStreamGetVolume(mystream);
info_paused = AStreamIsPaused(mystream);
info_pos = AStreamGetPosition(mystream);
info_seek = AStreamIsSeekable(mystream);
info_pitch = AStreamGetPitch(mystream);
info_pan = AStreamGetPan(mystream);
info_repeat_s = AStreamGetRepeat(mystream);
if info_playing
    {
    // Pitch Shift Functionality
    if keyboard_check(ord("Z")) 
    AStreamSetPitch(mystream,2)
    else if keyboard_check(ord("X"))
    AStreamSetPitch(mystream,0.5)
    else AStreamSetPitch(mystream,1);
    
    // Pan test
    if keyboard_check_direct(vk_lshift)
    AStreamSetPan(mystream,-1)
    else if keyboard_check_direct(vk_rshift)
    AStreamSetPan(mystream,1)
    else AStreamSetPan(mystream,0);
    
    // Fade out
    if keyboard_check_pressed(ord("C")) 
    && alarm[1] == -1
    && alarm[2] == -1 
    alarm[2] = 1;
    
    if keyboard_check_pressed(ord("V"))
        {
        AStreamSetRepeat(mystream,!AStreamGetRepeat(mystream));
        }
    }

// Pause functionality
if keyboard_check_pressed(vk_space)
    {
    if AStreamIsPlaying(mystream)
        {
        AStreamPause(mystream);
        }
    else
        {
        if AStreamIsPaused(mystream)
        AStreamResume(mystream)
        else 
            {
            mystream = AStreamPlay(myfile,0,1);
            AStreamSetRepeat(mystream,false);
            }
        }
    }

// Switch song
if keyboard_check_pressed(vk_enter)
    {
    mode += 1; if mode > 2 then mode = 0;
    AStreamStop(mystream);
    ASourceFree(myfile);
    event_user(mode);
    }
if keyboard_check_pressed(vk_backspace)
    {
    //show_message(AGetSupportedFileFormats());
    var f; f = get_open_filename("*.wav,*.ogg","");
    if f != ""
        {
        AStreamStop(mystream);
        ASourceFree(myfile);
        myfile = ASourceLoad(f);
        event_user(3);
        }
    }
    
// Kill game
if keyboard_check_pressed(vk_escape) then game_end();

rfps_avg += fps_real;

