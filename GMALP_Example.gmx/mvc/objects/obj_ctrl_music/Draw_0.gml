/// @description  Draw code
draw_set_font(fnt_default);
draw_set_color(c_white);
draw_set_halign(fa_left);

var bx = 5; 
var by = 5; 
var bz = 12;
draw_text(bx,by,string_hash_to_newline("Audiere v"+string(version)+" loaded")); by+=bz;
by+=bz;

draw_text(bx,by,string_hash_to_newline("*** SOURCE INFO ***")); by+=bz;
draw_text(bx,by,string_hash_to_newline(string(info_channels)+" channels, "+string(info_samplert)+" Hz")); by+=bz;
draw_text(bx,by,string_hash_to_newline("Length (from source): "+string(info_length)+" samples")); by+=bz;
draw_text(bx,by,string_hash_to_newline("Repeating: "+string(info_repeat)));by+=bz;
by+=bz;

draw_text(bx,by,string_hash_to_newline("*** LOOP INFO ***")); by+=bz;
if info_numloops > 0
    {
    var str,str2;
    for (var i=0;i<info_numloops;i+=1)
        {
        if mydata[i,2] == -1 then str2 = "infinitely" else str2 = string(mydata[i,2])+" time";
        if mydata[i,2] > 1 then str2 += "s";
        str = string(mydata[i,0])+", target: "+string(mydata[i,1])+", looping "+string(str2);
        draw_text(bx,by,string_hash_to_newline("Loop "+string(i)+" location: "+string(str))); 
        by+=bz;
        }
    }
else 
    {
    draw_text(bx,by,string_hash_to_newline("No loop points set for this track."));by+=bz;
    }
by+=bz;

draw_text(bx,by,string_hash_to_newline("*** STREAM INFO ***")); by+=bz;
draw_text(bx,by,string_hash_to_newline("Playing now: "+string(info_playing))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Volume: "+string(info_volume))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Paused: "+string(info_paused))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Seekable: "+string(info_seek))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Repeating: "+string(info_repeat_s))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Pitch Shift: "+string(info_pitch))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Pan: "+string(info_pan))); by+=bz;
draw_text(bx,by,string_hash_to_newline("Length (from stream): "+string(info_length2)+" samples")); by+=bz;
draw_text(bx,by,string_hash_to_newline("Position (from stream): "+string(info_pos)+" samples")); by+=bz;

by = room_height-12-(12*5);
draw_text(5,by,string_hash_to_newline("Z/X: alter pitch")); by+=bz;
draw_text(5,by,string_hash_to_newline("C: fade out, V: toggle repeat")); by+=bz;
draw_text(5,by,string_hash_to_newline("SHIFT keys to alter pan")); by+=bz;
draw_text(5,by,string_hash_to_newline("SPACE to pause/resume")); by+=bz;
draw_text(5,by,string_hash_to_newline("ENTER to change tracks")); by+=bz;

draw_set_halign(fa_right);
draw_text(room_width,room_height-15,string_hash_to_newline(string(rfps_str)+ " RFPS"));
draw_text(room_width,room_height-27,string_hash_to_newline(string(fps)+" FPS"));

