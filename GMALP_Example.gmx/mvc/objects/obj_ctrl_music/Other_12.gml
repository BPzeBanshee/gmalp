/// @description  Load test2.ogg

// CREATE SOURCE AND MANAGE IT
var ff = "sounds/test2.ogg";
myfile = ASourceLoad(ff);
if myfile == -1
    {
    show_message("File "+ff+" not found!");
    return -1;
    }

// Set TWO Loop Points
info_length = ASourceGetLength(myfile);
ASourceAddLoopPoint(myfile,240,123075,2);
ASourceAddLoopPoint(myfile,info_length*0.6670999130341582,info_length,-1);
mydata[0,0] = ASourceGetLoopPointData(myfile,0,0); // Loop point location
mydata[0,1] = ASourceGetLoopPointData(myfile,0,1); // Loop point target
mydata[0,2] = ASourceGetLoopPointData(myfile,0,2); // Amount of times to loop
mydata[1,0] = ASourceGetLoopPointData(myfile,1,0);
mydata[1,1] = ASourceGetLoopPointData(myfile,1,1);
mydata[1,2] = ASourceGetLoopPointData(myfile,1,2);

// Get additional info
info_numloops = ASourceGetNumLoopPoints(myfile);
info_channels = ASourceGetFormatData(myfile,0);
info_samplert = ASourceGetFormatData(myfile,1);

// CREATE AND RUN STREAM
mystream = AStreamPlay(myfile,1);
AStreamSetRepeat(mystream,true);
info_repeat = AStreamGetRepeat(mystream);
info_length2 = AStreamGetLength(mystream);

