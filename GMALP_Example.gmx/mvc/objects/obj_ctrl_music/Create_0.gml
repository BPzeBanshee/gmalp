// INITIALISATION
a = AInit();
if a < 0
    {
    show_message("AInit(): failed to initialise, exiting...");
    game_end();
    }
    
// DLL VERSION STRING
version = AVersion();

// Info getters
info_playing = false;
info_paused = false;
info_volume = 0;
info_pos = 0;
info_seek = 0;
info_pitch = 0;
info_pan = 0;
info_repeat = 0;
info_repeat_s = 0;

show_debug_message("Supported Audio Devices: "+string(AGetSupportedAudioDevices()));
show_debug_message("Supported File Formats: "+string(AGetSupportedFileFormats()));

// CALL EVENTS
mode = 0;
event_user(0);

// REAL FPS DISPLAY
rfps_avg = 0;
rfps_str = "----";
alarm[0] = 60;


