/// @description AStreamPlay(source_index,streaming,[volume])
/// @param source_index
/// @param streaming
/// @param [volume]
var v; v = 1;
if argument_count > 2 v = argument[2];

var e;
e = external_call(GMALP_StreamPlay, argument[0], argument[1], v);
return e;
